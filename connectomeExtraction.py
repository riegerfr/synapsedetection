import argparse

import napari
import nd2
import numpy as np
from scipy import ndimage

from detection import blur_threshold_size_segment, comp_nn


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--path",
        type=str,
        default="/home/franz/Downloads/220707_AAV 4thfloor 4x homer bassoon ba2amplified la_40x002.nd2",
        help="path to file",
    )
    parser.add_argument(
        "--pre_channel",
        type=int,
        default=0,
        help="pre channel (count from 0)",
    )
    parser.add_argument(
        "--post_channel",
        type=int,
        default=1,
        help="post channel (count from 0)",
    )
    parser.add_argument(
        "--neuron_channel",
        type=int,
        default=2,
        help="neuron channel (count from 0)",
    )
    parser.add_argument(
        "--pre_thr",
        type=int,
        default=800,
        help="pre threshold",
    )
    parser.add_argument(
        "--post_thr",
        type=int,
        default=200,
        help="post threshold",
    )
    parser.add_argument(
        "--neuron_thr",
        type=int,
        default=400,
        help="neuron threshold",
    )
    parser.add_argument(
        "--exp_fac",
        type=float,
        default=4,
        help="expansion factor",
    )
    args = parser.parse_args()
    return args


def get_neuron_for_synapse(syn_inst_seg, neuron_inst_seg, neuron_count):
    syn_inst_seg_overlap = syn_inst_seg + (neuron_inst_seg * 1j)
    # todo: float2int rounding dangerous -> keep as int initially?, how to avoid this hack while remaining
    #  fast/vectorized? Assign unique ID to each synapse/neuron pair (hash-like: pick two primes larger than
    #  both counts, multiply, risk overflow?) or use numpy array of tuples (stack+use axis)
    counts = np.unique(syn_inst_seg_overlap, return_counts=True)

    overlap_count_array = np.zeros((syn_inst_seg.max() + 1, neuron_count + 1))
    for id, count in zip(counts[0], counts[1]):
        overlap_count_array[np.int(np.real(id)), np.int(np.imag(id))] = count
    overlap_count_array[0, :] = 0  # remove background
    overlap_count_array[:, 0] = 0
    associated_id = np.argmax(overlap_count_array, axis=1)
    return associated_id[1:]  # list: length: # synapses, entries: range(1,#neurons+1)


def visualize(
        autapse_table,
        final_post_seg_aut,
        final_post_seg_syn,
        final_pre_seg_aut,
        final_pre_seg_syn,
        neuron_inst_seg,
        neuron_seg,
        nn_pre_post_raw,
        post_center,
        pre_center,
        raw,
        synapse_table,
        thr_post,
        thr_pre,
):
    n_viewer = napari.Viewer()
    n_viewer.add_image(raw, channel_axis=0, name=f"raw")
    n_viewer.add_labels(
        final_pre_seg_syn != 0, name=f"final_pre_syn", color={1: "green"}
    )
    n_viewer.add_labels(
        final_post_seg_syn != 0, name=f"final_post_syn", color={1: "pink"}
    )
    n_viewer.add_labels(
        final_pre_seg_aut != 0, name=f"final_pre_aut", color={1: "green"}
    )
    n_viewer.add_labels(
        final_post_seg_aut != 0, name=f"final_post_aut", color={1: "pink"}
    )
    # n_viewer.add_labels(final_pre_seg_syn, name=f"final_pre_inst", seed=3)
    # n_viewer.add_labels(final_post_seg_syn, name=f"final_post_inst", seed=3)
    n_viewer.add_labels(neuron_inst_seg, name=f"neuron_inst", seed=3, visible=False)
    n_viewer.add_labels(thr_pre, name=f"pre_thresh", color={1: "green"}, visible=False)
    n_viewer.add_labels(thr_post, name=f"post_thresh", color={1: "pink"}, visible=False)
    n_viewer.add_labels(neuron_seg, name=f"neuron_thresh", color={1: "yellow"})
    pre_post_locs_syn = np.array(
        [
            (pre_center[pre], post_center[post])
            for pre, post in enumerate(nn_pre_post_raw[1])
            if pre + 1 in synapse_table[0]
        ]
    )
    if len(pre_post_locs_syn) > 0:
        n_viewer.add_shapes(
            pre_post_locs_syn,
            name="connections_syn",
            shape_type="line",
            face_color="red",
            edge_color="red",
            edge_width=2,
        )
        pre_post_locs_aut = np.array(
            [
                (pre_center[pre], post_center[post])
                for pre, post in enumerate(nn_pre_post_raw[1])
                if pre + 1 in autapse_table[0]
            ]
        )
        n_viewer.add_shapes(
            pre_post_locs_aut,
            name="connections_aut",
            shape_type="line",
            face_color="red",
            edge_color="red",
            edge_width=2,
        )
    napari.run()


def get_masks(autapse_table, post_inst_seg, pre_inst_seg, synapse_table):
    final_pre_seg_syn = np.zeros_like(pre_inst_seg)
    pre_mask = np.isin(pre_inst_seg, synapse_table[0])
    final_pre_seg_syn[pre_mask] = pre_inst_seg[pre_mask]
    final_post_seg_syn = np.zeros_like(post_inst_seg)
    post_mask_syn = np.isin(post_inst_seg, synapse_table[2])
    final_post_seg_syn[post_mask_syn] = post_inst_seg[post_mask_syn]
    final_pre_seg_aut = np.zeros_like(pre_inst_seg)
    pre_mask_aut = np.isin(pre_inst_seg, autapse_table[0])
    final_pre_seg_aut[pre_mask_aut] = pre_inst_seg[pre_mask_aut]
    final_post_seg_aut = np.zeros_like(post_inst_seg)
    post_mask_aut = np.isin(post_inst_seg, autapse_table[2])
    final_post_seg_aut[post_mask_aut] = post_inst_seg[post_mask_aut]
    return final_post_seg_aut, final_post_seg_syn, final_pre_seg_aut, final_pre_seg_syn


def extract_synapses_autapses(
        both_nn_post_pre,
        both_nn_pre_post,
        neuron_count,
        nn_pre_post,
        post_to_neuron,
        pre_to_neuron,
):
    matched_pre_neurons = pre_to_neuron[both_nn_post_pre]
    matched_post_neurons = post_to_neuron[nn_pre_post[1]]
    autapse_mask = (
            (matched_pre_neurons == matched_post_neurons)
            & (matched_pre_neurons > 0)
            & (matched_post_neurons > 0)
    )
    print(f"num autapses {autapse_mask.sum()}")
    synapse_mask = (
            (matched_pre_neurons != matched_post_neurons)
            & (matched_pre_neurons > 0)
            & (matched_post_neurons > 0)
    )
    print(f"num synapses {synapse_mask.sum()}")
    pre_syn_id_neuron_id = np.array(
        [range(1, len(both_nn_post_pre) + 1), pre_to_neuron]
    )[:, both_nn_post_pre]
    post_syn_id_neuron_id = np.array(
        [range(1, len(both_nn_pre_post) + 1), post_to_neuron]
    )[:, nn_pre_post[1]]
    synapse_table = np.array(
        [
            pre_syn_id_neuron_id[0],
            pre_syn_id_neuron_id[1],
            post_syn_id_neuron_id[0],
            post_syn_id_neuron_id[1],
        ]
    )[:, synapse_mask]
    autapse_table = np.array(
        [
            pre_syn_id_neuron_id[0],
            pre_syn_id_neuron_id[1],
            post_syn_id_neuron_id[0],
            post_syn_id_neuron_id[1],
        ]
    )[:, autapse_mask]
    connectivity_matrix = np.zeros((neuron_count + 1, neuron_count + 1), dtype=int)
    for pre, post in zip(synapse_table[1], synapse_table[3]):
        connectivity_matrix[pre, post] += 1
    print(connectivity_matrix)
    return autapse_table, synapse_table


def compute_synapse_neuron_matching(
        both_nn_post_pre, both_nn_pre_post, neuron_seg, post_inst_seg, pre_inst_seg
):
    neuron_inst_seg, neuron_count = ndimage.label(neuron_seg)
    pre_to_neuron = get_neuron_for_synapse(pre_inst_seg, neuron_inst_seg, neuron_count)
    post_to_neuron = get_neuron_for_synapse(
        post_inst_seg, neuron_inst_seg, neuron_count
    )
    pre_with_neuron_count = (pre_to_neuron > 0).sum()
    post_with_neuron_count = (post_to_neuron > 0).sum()
    print(
        f"# pre with neuron {pre_with_neuron_count} of {len(pre_to_neuron)} (all pre),"
        f" ratio {pre_with_neuron_count / len(pre_to_neuron)}"
    )
    print(
        f"# post with neuron {post_with_neuron_count} of {len(post_to_neuron)} (all pre),"
        f" ratio {post_with_neuron_count / len(post_to_neuron)}"
    )
    pre_with_neuron_and_post = (pre_to_neuron[both_nn_post_pre] > 0).sum()
    print(
        f"# pre with neuron and post {pre_with_neuron_and_post} of"
        f" {len(pre_to_neuron[both_nn_post_pre])} (pre with post)"
    )
    print(
        f"# post with neuron and pre {(post_to_neuron[both_nn_pre_post] > 0).sum()} of"
        f" {len(post_to_neuron[both_nn_pre_post])}"
    )
    return neuron_count, neuron_inst_seg, post_to_neuron, pre_to_neuron


def get_segmentations(args, raw):
    high_count = 300

    print("neuron:")
    (
        neuron_seg,
        _,
    ) = blur_threshold_size_segment(  # todo: use FFNs for neuron instance segmentation
        raw[args.neuron_channel], args.neuron_thr, low_count=100, high_count=1e100
    )
    print(f"percentage of volume: {neuron_seg.mean()}")
    print("pre:")
    pre_seg, thr_pre = blur_threshold_size_segment(raw[args.pre_channel], args.pre_thr, high_count=high_count)
    print("post:")
    post_seg, thr_post = blur_threshold_size_segment(
        raw[args.post_channel], args.post_thr, high_count=high_count
    )
    return neuron_seg, post_seg, pre_seg, thr_post, thr_pre


def main():
    args = get_args()
    with nd2.ND2File(args.path) as f:
        raw = np.array(
            f.to_xarray()[
            :,
            :,
            500:1000,
            500:1000,  # todo: remove limitation, use dask for big scans
            ]
        ).swapaxes(0, 1)
        voxel_size = np.array(f.voxel_size())[[2, 0, 1]]  # need zxy
    print(
        f"volume in unexpanded tissue {np.cumprod(raw.shape[1:] * (voxel_size / args.exp_fac))[-1]} [µm³]"
    )

    # step 1: obtain segmentations
    neuron_seg, post_seg, pre_seg, thr_post, thr_pre = get_segmentations(args, raw)

    # step 2: match pre+post
    (
        both_nn_post_pre,  # todo: build in distance threshold here
        both_nn_pre_post,
        nn_post_pre,
        nn_post_pre_close,
        nn_pre_post,
        nn_pre_post_close,
        post_inst_seg,
        pre_inst_seg,
        selected_pre,
        selected_post,
        selected_pre_close,
        selected_post_close,
        voxel_vol,
        nn_pre_post_raw,
        nn_post_pre_raw,
        pre_center,
        post_center,
        both_nn_pre_post_close,  # todo: order correct?
        both_nn_post_pre_close,
    ) = comp_nn(post_seg, pre_seg, voxel_size=voxel_size, exp_fac=args.exp_fac)

    # step 3: match pre/post to neuron instances
    (
        neuron_count,
        neuron_inst_seg,
        post_to_neuron,
        pre_to_neuron,
    ) = compute_synapse_neuron_matching(
        both_nn_post_pre_close, both_nn_pre_post_close, neuron_seg, post_inst_seg, pre_inst_seg
    )
    # step 4: extract synapses (and "autapses")
    autapse_table, synapse_table = extract_synapses_autapses(
        both_nn_post_pre_close,
        both_nn_pre_post_close,
        neuron_count,
        nn_pre_post_close,
        post_to_neuron,
        pre_to_neuron,
    )

    # step 5: compute masks and visualize
    (
        final_post_seg_aut,
        final_post_seg_syn,
        final_pre_seg_aut,
        final_pre_seg_syn,
    ) = get_masks(autapse_table, post_inst_seg, pre_inst_seg, synapse_table)

    visualize(
        autapse_table,
        final_post_seg_aut,
        final_post_seg_syn,
        final_pre_seg_aut,
        final_pre_seg_syn,
        neuron_inst_seg,
        neuron_seg,
        nn_pre_post_raw,
        post_center,
        pre_center,
        raw,
        synapse_table,
        thr_post,
        thr_pre,
    )
    input()


if __name__ == "__main__":
    main()
