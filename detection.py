import argparse

import napari
import nd2
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from scipy import ndimage
from scipy.ndimage import gaussian_filter
from scipy.spatial import KDTree

#   conda install -c conda-forge nd2 seaborn matplotlib napari xarray
sns.set_context("poster")


def comp_nn(post_seg, pre_seg, voxel_size, exp_fac=3.5, close_thr=0.25):  # zyx
    pre_inst_seg = ndimage.label(pre_seg)[0]
    post_inst_seg = ndimage.label(post_seg)[0]
    pre_center = ndimage.center_of_mass(
        pre_seg, pre_inst_seg, np.arange(pre_inst_seg.max()) + 1
    )

    # pre_extrema = ndimage.extrema(
    #    pre_seg, pre_inst_seg, np.arange(pre_inst_seg.max()) + 1
    # )
    # areas = np.abs(np.cumprod(np.array(pre_extrema[2])- np.array(pre_extrema[3]), axis=1)[:,-1])

    post_center = ndimage.center_of_mass(
        post_seg, post_inst_seg, np.arange(post_inst_seg.max()) + 1
    )

    pixel_scales = np.array([list(voxel_size)]) / exp_fac
    pre_center_scaled = pre_center * pixel_scales
    post_center_scaled = post_center * pixel_scales
    pre_tree = KDTree(pre_center_scaled)
    post_tree = KDTree(post_center_scaled)
    nn_pre_post_raw = post_tree.query(pre_center_scaled)
    nn_post_pre_raw = pre_tree.query(post_center_scaled)
    print("nn_pre_post_raw [µm]: \n", describe(nn_pre_post_raw[0]))
    print("nn_post_pre_raw [µm]: \n", describe(nn_post_pre_raw[0]))
    both_nn_pre_post = nn_pre_post_raw[1][nn_post_pre_raw[1]] == np.arange(
        nn_post_pre_raw[1].shape[0]
    )
    both_nn_post_pre = nn_post_pre_raw[1][nn_pre_post_raw[1]] == np.arange(
        nn_pre_post_raw[1].shape[0]
    )
    assert both_nn_pre_post.sum() == both_nn_post_pre.sum()
    nn_pre_post = (
        nn_pre_post_raw[0][both_nn_post_pre],
        nn_pre_post_raw[1][both_nn_post_pre],
    )
    nn_post_pre = (
        nn_post_pre_raw[0][both_nn_pre_post],
        nn_post_pre_raw[1][both_nn_pre_post],
    )
    print("nn_pre_post filtered [µm]: \n", describe(nn_pre_post[0]))
    print("nn_post_pre filtered [µm]: \n", describe(nn_post_pre[0]))
    both_nn_pre_post_close = both_nn_post_pre & (nn_pre_post_raw[0] < close_thr)
    nn_pre_post_close = (
        nn_pre_post_raw[0][both_nn_pre_post_close],
        nn_pre_post_raw[1][both_nn_pre_post_close],
    )
    both_nn_post_pre_close = both_nn_pre_post & (nn_post_pre_raw[0] < close_thr)
    nn_post_pre_close = (
        nn_post_pre_raw[0][both_nn_post_pre_close],
        nn_post_pre_raw[1][both_nn_post_pre_close],
    )
    selected_pre = np.isin(pre_inst_seg, nn_post_pre[1] + 1)
    selected_post = np.isin(post_inst_seg, nn_pre_post[1] + 1)
    selected_pre_close = np.isin(pre_inst_seg, nn_post_pre_close[1] + 1)
    selected_post_close = np.isin(post_inst_seg, nn_pre_post_close[1] + 1)

    voxel_vol = pixel_scales[0, 0] * pixel_scales[0, 1] * pixel_scales[0, 2]

    return (
        both_nn_post_pre,
        both_nn_pre_post,
        nn_post_pre,
        nn_post_pre_close,
        nn_pre_post,
        nn_pre_post_close,
        post_inst_seg,
        pre_inst_seg,
        selected_pre,
        selected_post,
        selected_pre_close,
        selected_post_close,
        voxel_vol,
        nn_pre_post_raw,
        nn_post_pre_raw,
        pre_center,
        post_center,
        both_nn_post_pre_close,
        both_nn_pre_post_close,
    )


def blur_threshold_size_segment(raw, thr, low_count=5, high_count=1500, blur_sigma=1):
    blurred = gaussian_filter(raw.astype(np.float32),
                              blur_sigma) if blur_sigma > 0 else raw  # todo: anisotropy aware blurring
    thr_mask = blurred >= thr
    mask_filtered = filter_size(thr_mask, low_count, high_count)
    return mask_filtered, thr_mask


def filter_size(mask, low_count=5, high_count=1500):  # ~0.5 cubic microns
    # s = generate_binary_structure(3, 3)  # todo: why last =3, not 2 or 4?
    # featured_label, num_features = scipy.ndimage.label(
    ##    mask, structure=s
    # )  # -> find connected comps
    mask = mask.copy()
    featured_label, obj_count = ndimage.label(mask)

    ids_uni, counts = np.unique(featured_label, return_counts=True)
    count_mask_too_small = counts < low_count
    too_small_objs = ids_uni[count_mask_too_small]
    count_mask_too_big = counts > high_count
    too_big_objs = ids_uni[count_mask_too_big]
    print(
        f"# objects: {len(ids_uni)}, thereof too small {count_mask_too_small.sum()} , too big {count_mask_too_big.sum()}, remaining {len(ids_uni) - count_mask_too_small.sum() - count_mask_too_big.sum()}"
    )
    mask_too_small_objs = np.isin(featured_label, too_small_objs)
    mask[mask_too_small_objs] = 0
    mask_too_big_objs = np.isin(featured_label, too_big_objs)
    mask[mask_too_big_objs] = 0
    return mask
    # return s


def describe(x):
    return str(pd.Series(x).describe())


def visualize_napari_all(raw, selected_post, selected_pre, thr_pre, thr_post):
    n_viewer = napari.Viewer()
    n_viewer.add_image(raw, name=f"raw")
    n_viewer.add_labels(selected_pre, name=f"selected_pre")
    n_viewer.add_labels(selected_post, name=f"selected_post", seed=3)
    n_viewer.add_labels(thr_pre, name=f"pre_thresh", seed=3)
    n_viewer.add_labels(thr_post, name=f"post_thresh", seed=3)
    napari.run()
    n_viewer.show()


def plot_figs(
        both_nn_post_pre,
        both_nn_pre_post,
        nn_post_pre,
        nn_pre_post,
        post_inst_seg,
        pre_inst_seg,
        voxel_vol,
):
    p = sns.displot(
        nn_post_pre[0],
        kde=True,
        height=8,
        aspect=1,
    )  # xlabels="post_to_pre"
    p.fig.set_dpi(100)
    plt.xlabel("center of mass distance [µm]")
    plt.tight_layout()
    # displot(nn_pre_post[0],  # x="pre_to_post"
    #         )
    # plt.xlabel("pre_to_post")
    cube_vol = voxel_vol * pre_inst_seg.size
    density = nn_pre_post[0].shape[0] / cube_vol
    print(f"cube volume : {cube_vol} [µm³] , density: {density} [synapses/µm³]")
    plt.savefig("nn_dists.png")

    pre_voxel_vols = (
            np.unique(pre_inst_seg, return_counts=True)[1][1:][both_nn_post_pre] * voxel_vol
    )
    p = sns.displot(
        pre_voxel_vols,
        log_scale=True,
        kde=True,
        height=8,
        aspect=1,
    )
    p.fig.set_dpi(100)
    plt.xlabel("pre volumes [µm³]")
    plt.tight_layout()
    print("pre_voxel_vols [µm³]: \n", describe(pre_voxel_vols))
    plt.savefig("pre_vols.png")

    post_voxel_vols = (
            np.unique(post_inst_seg, return_counts=True)[1][1:][both_nn_pre_post]
            * voxel_vol
    )
    # plt.figure(figsize=(4, 4), dpi=160)
    p = sns.displot(
        post_voxel_vols,
        log_scale=True,
        kde=True,
        height=8,
        aspect=1,
    )
    p.fig.set_dpi(100)
    plt.xlabel("post volumes [µm³]")
    plt.tight_layout()
    # plt.text(1,1,s= describe(post_voxel_vols))
    print("post_voxel_vols [µm³]: \n", describe(post_voxel_vols))
    plt.savefig("post_vols.png")
    # plt.show()


def main():
    args = get_args()
    with nd2.ND2File(args.path) as f:
        raw = np.array(f.to_xarray()[:]).swapaxes(0, 1)
        voxel_size = np.array(f.voxel_size())[[2, 0, 1]]  # need zxy
    high_count = 200
    print("pre:")
    pre_seg, thr_pre = blur_threshold_size_segment(raw[args.pre_channel], args.pre_thr, high_count=high_count)
    print("post:")
    post_seg, thr_post = blur_threshold_size_segment(
        raw[args.post_channel], args.post_thr, high_count=high_count
    )

    (
        both_nn_post_pre,
        both_nn_pre_post,
        nn_post_pre,
        _,
        nn_pre_post,
        _,
        post_inst_seg,
        pre_inst_seg,
        selected_pre,
        selected_post,
        selected_pre_large,
        selected_post_large,
        voxel_vol,
        _,
        _,
        _,
        _,
    ) = comp_nn(post_seg, pre_seg, voxel_size=voxel_size, exp_fac=args.exp_fac)

    visualize_napari_all(raw, selected_post, selected_pre, thr_pre, thr_post)

    plot_figs(
        both_nn_post_pre,
        both_nn_pre_post,
        nn_post_pre,
        nn_pre_post,
        post_inst_seg,
        pre_inst_seg,
        voxel_vol,
    )

    # save_dataset(selected_post, selected_pre)

    print("done")
    input("Press enter to exit")


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--path",
        type=str,
        default="/home/franz/Downloads/from_sam/40x stack sven sample.nd2",
        help="path to file",
    )
    parser.add_argument(
        "--pre_channel",
        type=int,
        default=2,
        help="pre channel (count from 0)",
    )
    parser.add_argument(
        "--post_channel",
        type=int,
        default=3,
        help="post channel (count from 0)",
    )
    parser.add_argument(
        "--pre_thr",
        type=int,
        default=120 * 8,
        help="pre threshold",
    )
    parser.add_argument(
        "--post_thr",
        type=int,
        default=160 * 8,
        help="post threshold",
    )
    parser.add_argument(
        "--exp_fac",
        type=float,
        default=3.5,
        help="expansion factor",
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
