import numpy as np
from scipy.ndimage import center_of_mass
from skimage.measure import label


def synapse_det(pre, post, nis):
    pre_labeled = label(pre)
    post_labeled = label(post)
    pre_centers = center_of_mass(pre_labeled, pre_labeled, np.arange(pre_labeled.max()) + 1)