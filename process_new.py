import napari
import zarr

from detection import blur_threshold_size_segment, comp_nn

f = zarr.open('/home/franz/Downloads/230701_small_cube_all_data.zarr')

datasets = ['raw', 'labels']

for ds in datasets:
    print(ds)
    print(f[ds].shape, f[ds].dtype)
    print([i for i in f[ds].attrs])

# output:
# raw
# (18, 300, 300, 300) uint8
# ['axes', 'norm_vals', 'offset', 'resolution']
# labels
# (300, 300, 300) uint32
# ['axes', 'offset', 'resolution']

pre_thr = 0.1
post_thr = 0.1
pre_data = blur_threshold_size_segment(f['raw'][0], pre_thr, blur_sigma=0)[0]
post_data = blur_threshold_size_segment(f['raw'][1], post_thr, blur_sigma=0)[0]

# filter pre and post to only contain synapses with eaxctly one overlapping neuron label
pre_data, post_data =

comp_nn(pre_data, post_data)


print(f['raw'].attrs['norm_vals'])

n_viewer = napari.Viewer()
n_viewer.add_image(f['raw'], name='raw', channel_axis=0)
n_viewer.add_labels(f['labels'], name='labels')
napari.run()

input("done")
