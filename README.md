# SynapseDetection

This should install dependencies in your conda
enviornment: ```conda install -c conda-forge nd2 seaborn matplotlib napari xarray```

Run ```python detection.py --path /home/path/to/image.nd2 --pre_channel 2 --post_channel 3 --pre_thr 960 --post_thr 1280 --exp_fac 3.5```

Investigate in napari. (Deselect the thresholded labels.)

Find plots in the script's directory.

## Todo

- Cleanup
- Documentation