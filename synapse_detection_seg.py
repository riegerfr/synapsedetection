from collections import defaultdict

import napari
import numpy as np
import zarr
from scipy import ndimage


def independent_filter(arr, other_seg, thr=20, min_size=5, max_size=1000):
    # threshold
    arr_thr = arr >= thr

    # size filter
    labels, num = ndimage.label(arr_thr)
    sizes = np.bincount(labels.ravel())
    mask_size = (sizes >= min_size) & (sizes <= max_size)
    size_filtered = mask_size[labels]
    labels *= size_filtered

    # filter overlapping
    stacked = np.stack([labels.flatten(), other_seg.flatten()], axis=0)
    unique = np.unique(stacked, axis=1)  # slow!
    overlap_mask = np.array([np.sum((unique[0] == i) & (unique[1] != 0)) for i in range(num + 1)]) == 1
    labels *= overlap_mask[labels]

    print(f"found {num} objects, {np.sum(mask_size)} in size range, {overlap_mask.sum()} have neuron partner")
    return labels, num, unique


#
#
# def process_arrays(pre_raw, post_raw, seg, pre_thr=20, post_thr=20):
#     # todo: gaussian bluring on pre and post
#
#     pre_labels = independent_filter(pre_raw, seg, pre_thr)
#     post_labels = independent_filter(post_raw, seg, post_thr)
#
#     # Filter objects that overlap with exactly one unique object in seg
#     pre_overlap = [np.unique(seg[pre_labels == i]) for i in range(1, pre_num + 1)]
#     post_overlap = [np.unique(seg[post_labels == i]) for i in range(1, post_num + 1)]
#
#     pre_labels = np.array([len(x) == 1 for x in pre_overlap])[pre_labels - 1] * pre_labels
#     post_labels = np.array([len(x) == 1 for x in post_overlap])[post_labels - 1] * post_labels
#
#     # Compute center of mass
#     pre_com = ndimage.center_of_mass(pre, pre_labels, range(1, pre_num + 1))
#     post_com = ndimage.center_of_mass(post, post_labels, range(1, post_num + 1))
#
#     # Compute nearest neighbors
#     tree = KDTree(post_com)
#     pre_dists, pre_inds = tree.query(pre_com, k=1)
#
#     tree = KDTree(pre_com)
#     post_dists, post_inds = tree.query(post_com, k=1)
#
#     # Filter mutual nearest neighbors
#     pre_mask_nn = np.arange(pre_num) == post_inds[pre_inds]
#     post_mask_nn = np.arange(post_num) == pre_inds[post_inds]
#
#     pre_labels = pre_mask_nn[pre_labels - 1] * pre_labels
#     post_labels = post_mask_nn[post_labels - 1] * post_labels
#
#     # Filter where associated ID in seg is different
#     pre_ids = seg[pre_labels - 1]
#     post_ids = seg[post_labels - 1]
#
#     pre_filtered = (pre_ids != post_ids[pre_inds]) * pre_labels
#     post_filtered = (post_ids != pre_ids[post_inds]) * post_labels
#
#     return pre_filtered, post_filtered
#

f = zarr.open('/home/franz/Downloads/230701_small_cube_all_data.zarr')

# result = process_arrays(f['raw'][0], f['raw'][1], f['labels'][:])
seg = f['labels'][:]
pre_labels, num_pre, pre_seg_unique = independent_filter(f['raw'][0], seg, 10)
post_labels, num_post, post_seg_unique = independent_filter(f['raw'][1], seg, 10)

# pre_labels, num_pre = ndimage.label(pre_labels)
# post_labels, num_post = ndimage.label(post_labels)

stacked = np.stack([pre_labels.flatten(), post_labels.flatten()], axis=0)
unique = np.unique(stacked, axis=1)  # slow!

pre_to_post = defaultdict(set)
post_to_pre = defaultdict(set)

for pre_id, post_id in unique.T:
    if pre_id == 0 or post_id == 0:
        continue
    pre_to_post[pre_id].add(post_id)
    post_to_pre[post_id].add(pre_id)

pre_to_post_selected = {k: list(v)[0] for k, v in pre_to_post.items() if len(v) == 1}
post_to_pre_selected = {k: list(v)[0] for k, v in post_to_pre.items() if len(v) == 1}
# keep only mutually same
pre_to_post_selected = {k: v for k, v in pre_to_post_selected.items() if
                        v in post_to_pre_selected and post_to_pre_selected[v] == k}
post_to_pre_selected = {k: v for k, v in post_to_pre_selected.items() if
                        v in pre_to_post_selected and pre_to_post_selected[v] == k}

pre_mask = np.array([i in pre_to_post_selected for i in range(num_pre + 1)])
post_mask = np.array([i in post_to_pre_selected for i in range(num_post + 1)])

print(f"found {len(pre_to_post_selected)} pre <-> post pairs")

pre_labels *= pre_mask[pre_labels]
post_labels *= post_mask[post_labels]

pre_preselected = np.copy(pre_labels)
post_preselected = np.copy(post_labels)

assert len(post_to_pre_selected) == len(pre_to_post_selected)
assert pre_to_post_selected == {v: k for k, v in post_to_pre_selected.items()}

# pre_seg_unique = np.unique(np.stack([pre_labels.flatten(), seg.flatten()], axis=0), axis=1)
pre_to_seg = {pre_id: seg_id for pre_id, seg_id in pre_seg_unique.T if pre_id != 0 and seg_id != 0}
# assert pre_to_seg.keys() == pre_to_post_selected.keys()
# post_seg_unique = np.unique(np.stack([post_labels.flatten(), seg.flatten()], axis=0), axis=1)
post_to_seg = {post_id: seg_id for post_id, seg_id in post_seg_unique.T if post_id != 0 and seg_id != 0}
# assert post_to_seg.keys() == post_to_pre_selected.keys()


# for pre_id, post_id in pre_to_post_selected.items():
#    print(
#        f"pre neuron {pre_to_seg[pre_id]} -> post neuron {post_to_seg[post_id]} (pre/post synapse ids {pre_id} -> {post_id})")

pre_to_post_seg_filtered = {pre_id: pre_to_post_selected[pre_id] for pre_id, post_id in pre_to_post_selected.items() if
                            pre_to_seg[pre_id] != post_to_seg[post_id]}
post_to_pre_seg_filtered = {post_id: post_to_pre_selected[post_id] for post_id, pre_id in post_to_pre_selected.items()
                            if
                            post_to_seg[post_id] != pre_to_seg[pre_id]}
assert pre_to_post_seg_filtered == {v: k for k, v in post_to_pre_seg_filtered.items()}

pre_seg_filtered_mask = np.array([i in pre_to_post_seg_filtered for i in range(num_pre + 1)])
post_seg_filtered_mask = np.array([i in post_to_pre_seg_filtered for i in range(num_post + 1)])

print(f"found {len(pre_to_post_seg_filtered)} pre <-> post pairs with different seg id")

pre_labels *= pre_seg_filtered_mask[pre_labels]
post_labels *= post_seg_filtered_mask[post_labels]

connectivity_matrix = np.zeros((seg.max() + 1, seg.max() + 1), dtype=np.int32)
for pre_id, post_id in pre_to_post_seg_filtered.items():
    connectivity_matrix[pre_to_seg[pre_id], post_to_seg[post_id]] += 1
    print(
        f"pre neuron {pre_to_seg[pre_id]} -> post neuron {post_to_seg[post_id]} (pre/post synapse ids {pre_id} -> {post_id})")
print(connectivity_matrix)


# plot connectivity matrix
def plot():
    import matplotlib.pyplot as plt
    plt.imshow(connectivity_matrix)
    plt.show()


# plot()


synapses = (pre_labels > 0) | (post_labels > 0)
synapse_labels, num = ndimage.label(synapses)
# stacked = np.stack([synapse_labels.flatten(), seg.flatten()], axis=0)
# unique = np.unique(stacked, axis=1)  # slow!
# overlap_mask = np.array([np.sum((unique[0] == i) & (unique[1] != 0)) for i in range(num + 1)]) == 2  # exactly 2 labels
# synapse_labels *= overlap_mask[synapse_labels]

# overlap_mask = np.array([np.sum((unique[0] == i) & (unique[1] != 0)) for i in range(num + 1)]) == 1
# labels *= overlap_mask[labels]

n_viewer = napari.Viewer()
n_viewer.add_image(f['raw'], name='raw', channel_axis=0, visible=False)
n_viewer.add_labels(f['labels'], name='labels')
n_viewer.add_labels(pre_labels > 0, name='pre', color={1: 'orange'})
n_viewer.add_labels(pre_labels, name='pre_labels', visible=False)
n_viewer.add_labels(post_labels > 0, name='post', color={1: 'blue'})
n_viewer.add_labels(post_labels, name='post_labels', visible=False)
n_viewer.add_labels(synapses, name='synapses', visible=False, )
n_viewer.add_labels(synapse_labels > 0, name='synapse', visible=False)
n_viewer.add_labels(synapse_labels, name='synapse_labels', visible=False)

n_viewer.add_labels(pre_preselected > 0, name='pre_preselected', visible=True, color={1: 'orange'})
n_viewer.add_labels(post_preselected > 0, name='post_preselected', visible=True, color={1: 'blue'})

napari.run()

print("done")
